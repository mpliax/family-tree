from __future__ import annotations  # For type hinting before class declaration
from enum import Enum
from datetime import datetime
from dateutil.relativedelta import relativedelta


class GENDER(Enum):
    FEMALE = "FEMALE"
    MALE = "MALE"

    @classmethod
    def get_value(cls, value):
        for option in cls:
            if option.value == value:
                return option
            else:
                return None


class FamilyMember(object):
    """Class describing a family member and its relationships"""

    UNIQUE_ID = 0

    def __init__(
        self,
        name: str = "",
        surname: str = "",
        gender: GENDER = GENDER.FEMALE,
        birthdate: datetime = datetime.now(),
        deathdate: datetime = None,
        is_deceased: bool = False,
        portrait: str = None,
    ):

        self.name = name
        self.surname = surname
        self._married_to = None
        self.gender = gender
        self._mother = None
        self._father = None
        self.children = []
        self.birthdate = birthdate
        self.deathdate = deathdate
        self.is_deceased = is_deceased
        self.portrait = portrait
        self._id = FamilyMember.UNIQUE_ID
        FamilyMember.UNIQUE_ID += 1

    @property
    def married_to(self) -> FamilyMember:
        return self._married_to

    @married_to.setter
    def married_to(self, person: FamilyMember):
        if person is not None and not type(person) == FamilyMember:
            raise TypeError(
                f"{person} is of type {type(person)} but it should be a FamilyMember"
            )
        self._married_to = person
        if person is not None and person.married_to is None:
            person.married_to = self

    @property
    def is_married(self) -> bool:
        return self._married_to is not None

    @property
    def mother(self):
        return self._mother

    @mother.setter
    def mother(self, person):
        if person is not None and not type(person) == FamilyMember:
            raise TypeError(
                f"{person} is of type {type(person)} but it should be a FamilyMember"
            )
        if person is not None and self not in person.children:
            person.children.append(self)
        self._mother = person

    @property
    def father(self):
        return self._father

    @father.setter
    def father(self, person):
        if person is not None and not type(person) == FamilyMember:
            raise TypeError(
                f"{person} is of type {type(person)} but it should be a FamilyMember"
            )
        if person is not None and self not in person.children:
            person.children.append(self)
        self._father = person

    @property
    def num_children(self):
        return len(self.children)

    @property
    def age(self):
        """In years"""
        return (
            relativedelta(self.deathdate, self.birthdate).years
            if self.deathdate
            else relativedelta(datetime.now(), self.birthdate).years
        )

    @property
    def short_name(self):
        return f"{self.name[0:2]}. {self.surname} ({self.birthdate.year})"

    @property
    def full_name(self):
        return f"{self.name}. {self.surname} ({self.birthdate.year})__{self._id}"

    @property
    def honorific(self):
        if self.gender == GENDER.FEMALE:
            if self.is_married:
                honorific = "Mrs."
            else:
                honorific = "Ms."
        else:
            honorific = "Mr."
        return honorific

    def __str__(self):
        return f"{self.honorific} {self.name} {self.surname} ({self.age}){' married to '+ self.married_to.name if self.is_married else ''}{' (' + str(len(self.children)) + ' children)' if len(self.children) > 0 else ''}"
