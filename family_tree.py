import pygraphviz as pgv
from datetime import datetime
import os.path
from family_member import FamilyMember, GENDER
import csv

IN_FILEPATH = "input.csv"
OUT_DIR = "IMG"
OUT_FILENAME = "family_tree.png"
OUT_PATH = os.path.join(OUT_DIR, OUT_FILENAME)


def draw_family_tree(family_dict):
    """Given a single FamilyMember, follow relationships downwards and draw the family tree"""

    def get_group(generation):
        return f"gen{generation}"

    def add_family_member_node(graph, person: FamilyMember, generation=0):
        """Function that draws a single node for supplied FamilyMember"""
        graph.add_node(
            person.full_name,
            color="gray" if person.is_deceased else "black",
            fontcolor="gray" if person.is_deceased else "black",
            style="dashed" if person.is_deceased else "",
            shape="box",
            group=get_group(generation),
            image=person.portrait if person.portrait is not None else "",
        )
        return graph

    def follow_tree(person: FamilyMember, graph: pgv.AGraph, generation: int = 0):
        """Given a starting FamilyMember, draw all relationships for following generations"""
        # print(generation, person.short_name, person.is_deceased)
        graph = add_family_member_node(graph, person, generation)

        if person.is_married:
            graph = add_family_member_node(graph, person.married_to, generation)
            graph.add_edge(
                person.full_name,
                person.married_to.full_name,
                arrowhead="none",
                color="red",
                rank="same",
                constraint=False,
            )
        for child in person.children:
            graph = add_family_member_node(graph, child, generation)
            graph.add_edge(person.full_name, child.full_name)
            if person.is_married:
                graph.add_edge(person.married_to.full_name, child.full_name)
            graph = follow_tree(child, graph, generation + 1)
        return graph

    # Create graph
    G = pgv.AGraph(
        directed=True, strict=True, rankdir="TB", ratio="auto", overlap="scale"
    )
    for k, person in family_dict.items():
        if person is not None and not type(person) == FamilyMember:
            raise TypeError(
                f"{person} is of type {type(person)} but it should be a FamilyMember"
            )
        # Add nodes as needed, starting from a single person
        G = follow_tree(person, G)

    G.layout()
    G.draw(OUT_PATH)
    G.write(OUT_PATH + ".dot")


def import_data(csv_filepath):
    """
    Import data from the supplied CSV filepath

    Required columns:
    id, name, surname, gender, birth_year, birth_month, birth_day, father,
    mother, married_to, is_deceased, portrait.


    """
    family = {}
    orphan_members = {}

    def process_orphan_relationships(family_dict: dict, orphans_dict: dict):
        """A second pass for fixing orphan relationships after reading the CSV"""
        for p_id, attrs in orphans_dict.items():
            for attr in attrs:
                setattr(family_dict[p_id], attr, family[orphans_dict[p_id][attr]])
        return family_dict

    with open(csv_filepath) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            try:
                p_id = int(row["id"])
                p = FamilyMember(
                    name=row["name"],
                    surname=row["surname"],
                    gender=GENDER.get_value(row["gender"]),
                    birthdate=datetime(
                        int(row["birth_year"]),
                        int(row["birth_month"]),
                        int(row["birth_day"]),
                    ),
                    is_deceased=True if row["death_year"] else False,
                    portrait=row["portrait"],
                )
                # potrait=row['portrait'])
                # Read and set relationships
                for attr in ["married_to", "mother", "father"]:
                    try:
                        m = int(row[attr])
                        setattr(p, attr, family[m])
                    except ValueError as e:
                        print(repr(e))
                        # Empty string
                        continue
                    except KeyError:
                        # Referenced member that's not yet read from csv
                        if p_id not in orphan_members.keys():
                            orphan_members[p_id] = dict()
                        orphan_members[p_id][attr] = m
                    except Exception as e:
                        print(repr(e))

                # Add FamilyMember to family dict
                family[p_id] = p

            except Exception as e:
                print(repr(e))
                pass
    family = process_orphan_relationships(family, orphan_members)
    return family


def main():
    family_dict = import_data(IN_FILEPATH)
    draw_family_tree(family_dict)


if __name__ == "__main__":
    main()
